from app import create_app, db
from app.models import MicroEmpresa, Apoiar

app = create_app()

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'MicroEmpresa': MicroEmpresa, 'Apoiar': Apoiar}

