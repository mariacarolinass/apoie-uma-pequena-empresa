#!/usr/bin/env python# -*- coding: utf-8 -*-
import sys
from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app, Response
from flask_babel import _, get_locale
from app import db
from app.main.form import MicroEmpresaForm
from app.models import MicroEmpresa, Apoiar
from app.main import bp

@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    microempresa = MicroEmpresa.query.order_by(MicroEmpresa.timestamp.desc()).all()
    return render_template('index.html', title=(_('Página Inicial')), microempresa=microempresa)

@bp.route('/explore', methods=['GET', 'POST'])
def explore():
    microempresa = MicroEmpresa.query.order_by(MicroEmpresa.timestamp.desc()).all()
    return render_template('explore.html', title=(_('Explorar')), microempresa=microempresa)

@bp.route('/register_microempresa', methods=['GET', 'POST'])
def register_microempresa():
    form = MicroEmpresaForm()
    if form.validate_on_submit():
        microempresa = MicroEmpresa(title=form.title.data, description=form.description.data,
        goal=form.goal.data, location=form.location.data)
        db.session.add(microempresa)
        db.session.commit()
        flash(_('Cadastro realizado com sucesso!'))
    return render_template('register_microempresa.html', title=(_('Cadastrar Ideia')), form=form)

@bp.route('/edit_microempresa/<int:id>', methods=['GET', 'POST'])
def edit_microempresa(id):
    microempresa = MicroEmpresa.query.get_or_404(id)
    form = MicroEmpresaForm()
    if form.validate_on_submit():
        microempresa.title = form.title.data
        microempresa.description = form.description.data
        microempresa.goal = form.goal.data
        microempresa.location = form.location.data
        db.session.add(microempresa)
        db.session.commit()
        flash(_('As alterações foram salvas!'))
    form.title.data = microempresa.title
    form.description.data = microempresa.description
    form.goal.data = microempresa.goal
    form.location.data = microempresa.location
    return render_template('edit_microempresa.html', title=(_('Editar Ideia')),
                           form=form, microempresa=microempresa)

@bp.route("/deletar_microempresa/<int:id>")
def deletar_microempresa(id):
    microempresa = MicroEmpresa.query.filter_by(id=id).first()
    db.session.delete(microempresa)
    db.session.commit()
    flash(_('Apagado com sucesso!'))
    return redirect(url_for("main.index"))

@bp.route('/microempresa_profile/<title>', methods=['GET', 'POST'])
def microempresa_profile(title):
    microempresa = MicroEmpresa.query.filter_by(title=title).first_or_404()
    return render_template('microempresa_profile.html', title=(_('Perfil da Ideia')),
        microempresa=microempresa)

@bp.route('/about', methods=['GET', 'POST'])
def about():
    return render_template('about.html', title=(_('Sobre')))
