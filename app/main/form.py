#!/usr/bin/env python -*- coding: utf-8 -*-
from flask import request
from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Length
from datetime import datetime
from flask_babel import _, lazy_gettext as _l
from app.models import MicroEmpresa, Apoiar


class MicroEmpresaForm(FlaskForm):
	title = StringField(('Título:'), validators=[DataRequired(),
		Length(min=3)], render_kw={"placeholder": "Digite um título"})
	description = TextAreaField(('Descrição:'), validators=[DataRequired(), Length(max=550)],
		render_kw={"rows": 6, "placeholder": "Digite uma breve descrição"})
	goal = StringField(('Meta Financeira: R$'), validators=[DataRequired()],
		render_kw={"placeholder": "Digite o valor da meta que deseja cumprir"})
	location = StringField(('Localização:'), validators=[DataRequired()],
		render_kw={"placeholder": "Digite a localização da sua pequena ou micro empresa"})
	submit = SubmitField(('Cadastrar'))
