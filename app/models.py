#!/usr/bin/env python -*- coding: utf-8 -*-
from datetime import datetime
from hashlib import md5
from time import time
from flask import current_app
from app import db


class MicroEmpresa(db.Model):

	__tablename__ = 'microempresa'
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(200), index=True, unique=True)
	location = db.Column(db.String(200), index=True)
	goal = db.Column(db.String(200), index=True, unique=True)
	description = db.Column(db.String(800), index=True)
	timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
	apoio = db.relationship(
        'Apoiar',
        foreign_keys='Apoiar.microempresa_id',
        backref='microempresa', lazy='dynamic')

	def like_microempresa(self, apoiar):
		if not self.has_liked_microempresa(apoiar):
			like = Apoiar(microempresa_id=self.id, apoiar_id=apoiar.id)
			db.session.add(like)

	def has_liked_microempresa(self, apoiar):
		return Apoiar.query.filter(
			Apoiar.microempresa_id == self.id,
			Apoiar.apoiar_id == apoiar.id).count() > 0

	def __repr__(self):
		return '{}'.format(self.title)


class Apoiar(db.Model):

	id = db.Column(db.Integer, primary_key=True)
	apoiar = db.Column(db.String(200), index=True)
	timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

	microempresa_id = db.Column(db.Integer, db.ForeignKey('microempresa.id'))
	apoiar_id = db.Column(db.Integer, db.ForeignKey('apoiar.id'))

	def __repr__(self):
		return '{}'.format(self.apoiar)
